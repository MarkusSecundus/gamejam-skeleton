# Change Log
All notable changes to this project will be documented in this file.

## [Unreleased] - 2024-02-12

Here we write upgrading notes for brands. It's a team effort to make them as
straightforward as possible.

### Added
- [Create 3D URP setup branch](https://gitlab.com/trampod/gamejam-skeleton/-/issues/9) -
 Added 3D Renderer and added it to settings. Created example scene for 3D.
- [Add Basic Audiomanager](https://gitlab.com/trampod/gamejam-skeleton/-/issues/9) -
  Added basic audio settings for music and sfx and an example scene for it

### Changed
- [Solve Singleton Conflict](https://gitlab.com/trampod/gamejam-skeleton/-/issues/8) -
  Renamed Singleton to SmartSingleton.

### Fixed

## [GamejamSkeleton 1.0] - 2023-11-24

### Added

- Imported DOTween
- Imported ExecutionOrderAttribute
- Imported New Input System
- Imported TextMesh Pro
- Setuped WebGL build
- Input Example
- Imported MyBox
- Imported Recorder
- Custom Singleton class
- List Extensions
- Array, Color, Random, Vector Extensions
- Color Helper

### Changed

### Fixed
