# Unity Gamejam Skeleton

This Unity project is meant to be a starting point for use at gamejams.

It is meant as an already setup project without any precoded gameplay stuff. There might be some general infrastructure parts, but other than that it should be a blank paper with all drawing equipment ready to use.

## [Wiki](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/home)

There is a more detailed [wiki](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/home) explaining this project in greater detail.


## [Content (included packages)](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/included-packages)

List of all imported packages used in this Game Jam Skeleton.

- [DOTween](Packages/DOTween)
- [ExecutionOrderAttribute](Packages/Execution-Order-Attribute)
- [(New) Input System](Packages/New-Input-System)
- [TextMesh Pro](Packages/TextMesh-Pro)
- [MyBox](Packages/MyBox)
- [Cinemachine](Packages/Cinemachine)
- [Recorder](Packages/Recorder)

### [Utils](https://gitlab.com/trampod/gamejam-skeleton/-/wikis/utils)

Project contains few custom additions. This includes custom premade singleton programming pattern and some extensions and helpers.

## Contributors

<!-- omit in toc -->
### Trampod (Original Creator)

[itch.io](https://trampod.itch.io)

<!-- omit in toc -->
### MianenCZ

[mianen.cz](https://mianen.cz)

[itch.io](https://mianencz.itch.io)

[buy me a coffee](https://www.buymeacoffee.com/MianenCZ)

<!-- omit in toc -->
### Quanti s.r.o.

[<img src="https://gitlab.com/trampod/gamejam-skeleton/-/wikis/Resources/quanti-logo.png" alt="quanti.cz" width="300"/>](https://www.quanti.cz/)

[quanti.cz](https://www.quanti.cz/)

